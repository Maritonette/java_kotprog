﻿import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class IKerdes extends Kerdes {
	private int helyes;
	String igaz;
	String hamis;

	/**
	 * Konstruktor beallitja az igaz es hamis valaszokat es a helyes valaszt. A
	 * valaszokat egy tombben tarolja.
	 * 
	 * @param kerdes
	 * @param helyes
	 */
	public IKerdes(String kerdes, int helyes) {
		super(kerdes);
		this.helyes = helyes;
		igaz = "A:Igaz";
		hamis = "B:Hamis";
	}

	public int getHelyes() {
		return helyes;
	}

	@Override
	/**
	 * toString felteszi a kerdest.
	 */
	public String toString() {
		return kerdes + "\r\n" + igaz + "\r\n" + hamis + "\r\n";
	}

	/**
	 * A szulo kerdez metodusanak megvalositasa. Felteszi a kerdest es valaszt
	 * var.A beker metodusbol kapja a parancsot. Megkapja a meccs nevu Jatek
	 * peldanyt is hogy tudja ellenorzni a segitsegeket.
	 */
	public boolean kerdez(Jatek meccs) {
		String parancs = "";
		char valasz;
		while (true) {
			System.out.println(this);
			parancs = beker();

			if (parancs.equals("felez") && meccs.isFelezo()) {
				felez();
				meccs.setFelezo(false);
			}
			if (parancs.equals("telefon") && meccs.isTelefon()) {
				telefon();
				meccs.setTelefon(false);
			}
			if (parancs.equals("kozonseg") && meccs.isKozonseg()) {
				kozonseg();
				meccs.setKozonseg(false);
			}
			if (parancs.equals("A") || parancs.equals("B")) {

				valasz = parancs.charAt(0);
				if (valasz - 'A' + 1 == helyes) {
					return true;
				} else {
					System.out.println("A valasz helytelen. A helyes valasz: "
							+ (char) (helyes + 'A' - 1));
					return false;
				}
			}
		}

	}

	@Override
	/**
	 * A Kerdes beker metodusanak megvalositasa.
	 */
	protected String beker() {
		String parancs = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (!parancs.equals("A") && !parancs.equals("B")
				&& !parancs.equals("felez") && !parancs.equals("kozonseg")
				&& !parancs.equals("telefon")) {
			System.out.println("Gepelje be a valasz betujelet.");
			try {
				parancs = br.readLine();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return parancs;
	}

	@Override
	/**
	 * Eltunteti a rossz megoldast.
	 */
	protected void felez() {
		if (helyes == 1) {
			hamis = "";
		} else {
			igaz = "";
		}

	}

	@Override
	/**
	 * A szulo kozonseg metodusanak megvalositasa. 
	 * Megvalositas 20-100 kozott general 1 veletlenszeru szamot, ezt a szamot irja a jo megoldas vegere
	 *  a maradekot a helytelen megoldas vegere. Igy a helyes megoldasnal nagyobb esellyel fog nagyobb 
	 *  szam szerepelni.
	 */
	protected void kozonseg() {
		Random rand = new Random();
		int n = 20 + rand.nextInt(100);
		System.out.println(kerdes + "\r\n" + igaz + " " + n + "\r\n" + hamis
				+ " " + (100 - n) + "\r\n");

	}
	@Override
	/**
	 * A szulo telefon metodusanak megvalositasa. Eltunteti a rossz
	 * megoldasokat.
	 */
	protected void telefon() {
		if (helyes == 1) {
			hamis = "";
		} else {
			igaz = "";
		}
	}

}
