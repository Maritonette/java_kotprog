﻿import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Fomenu {
	/**
	 * Beker 1 parancsot es meghivja az annak megfelelo metodust.
	 */
	public static void indit() {
		Adat.osszegek();
		String parancs = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Gepeld be a 'start' parancsot az indulashoz.");
		while (!parancs.equals("start")) {
			try{
				parancs=br.readLine();	
			}catch(Exception e){
				
			}
		}
		if(parancs.equals("start")){
			ujjatek();
		}
		
	}
	/**
	 * Az Adat class static metodusai segitsegevel beolvassa a kerdeseket a class static attributumaiba.
	 * Majd peldanyositja a Jatek classt azaz elinditja a jatekot.
	 */
	public static void ujjatek() {
		Adat.beolvas();
		new Jatek();
	}
}
