﻿import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class FKerdes extends Kerdes {
	private String[] valasz;
	private int helyes;

	/**
	 * Konstruktor beallitja a kerdes erteket a valaszokat es a helyes valaszt.
	 * A valaszokat egy tombben tarolja.
	 * 
	 * @param kerdes
	 *            Kerdes.
	 * @param valasz1
	 * @param valasz2
	 * @param valasz3
	 * @param valasz4
	 * @param helyes
	 */
	public FKerdes(String kerdes, String valasz1, String valasz2,
			String valasz3, String valasz4, int helyes) {
		super(kerdes);
		valasz = new String[4];
		this.valasz[0] = valasz1;
		this.valasz[1] = valasz2;
		this.valasz[2] = valasz3;
		this.valasz[3] = valasz4;
		this.helyes = helyes;
	}

	/**
	 * A Kerdes beker metodusanak megvalositasa.
	 */
	protected String beker() {
		String parancs = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (!parancs.equals("A") && !parancs.equals("B")
				&& !parancs.equals("C") && !parancs.equals("D")
				&& !parancs.equals("felez") && !parancs.equals("kozonseg")
				&& !parancs.equals("telefon")) {
			System.out
					.println("Gepelje be a valasz betujelet vagy a'felez'szot a felezeshez,'kozonseg' szot a kozonseg segitsegeert,'telefon' szot a telefonos segitseghez");
			try {
				parancs = br.readLine();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return parancs;
	}

	/**
	 * A szulo kerdez metodusanak megvalositasa. Felteszi a kerdest es valaszt
	 * var.A beker metodusbol kapja a parancsot. Megkapja a meccs nevu Jatek
	 * peldanyt is hogy tudja ellenorzni a segitsegeket.
	 */
	public boolean kerdez(Jatek meccs) {
		String parancs = "";
		char valasz;
		while (true) {
			System.out.println(this);
			parancs = beker();
			if (parancs.equals("felez") && meccs.isFelezo()) {
				felez();
				meccs.setFelezo(false);
			}
			if (parancs.equals("telefon") && meccs.isTelefon()) {
				telefon();
				meccs.setTelefon(false);
			}
			if (parancs.equals("kozonseg") && meccs.isKozonseg()) {
				kozonseg();
				meccs.setKozonseg(false);
			}
			if (parancs.equals("A") || parancs.equals("B")
					|| parancs.equals("C") || parancs.equals("D")) {

				valasz = parancs.charAt(0);
				if (valasz - 'A' + 1 == helyes) {
					return true;
				} else {
					System.out.println("A valasz helytelen. A helyes valasz: "
							+ (char) (helyes + 'A' - 1));
					return false;
				}
			}
		}
	}

	/**
	 * A szulo kozonseg metodusanak megvalositasa. Megvalositas:0-maradek kozott
	 * general egy veletlen szamot.A maradek kezdetben 100, majd a generalt
	 * szamokat mindig kivonjuk a maradekbol, es ugy generalja a kovetkezot.
	 */
	protected void kozonseg() {
		int i;
		int[] szazalek = new int[4];
		Random rand = new Random();
		int maradek = 100;
		szazalek[helyes - 1] = rand.nextInt(maradek);
		maradek -= szazalek[helyes - 1];
		for (i = 0; i <= 3; i++) {
			if (i != helyes - 1) {
				szazalek[i] = rand.nextInt(maradek);
				maradek -= szazalek[i];
			}
		}
		szazalek[i - 1] += maradek;
		for (int j = 0; j < 4; j++) {
			System.out.println(valasz[j] + " " + szazalek[j]);
		}
	}

	/**
	 * A szulo felez metodusanak megvalositasa. Veletlenszeruen eltuntet 2 rossz
	 * megoldast.
	 */
	protected void felez() {
		Random rand = new Random();
		int n = 1 + rand.nextInt(4);
		for (int i = 0; i < 2; i++) {
			while (n == helyes || valasz[n - 1].equals("")) {
				n = 1 + rand.nextInt(4);
			}
			valasz[n - 1] = "";
		}
	}

	/**
	 * A szulo telefon metodusanak megvalositasa. Eltunteti a rossz
	 * megoldasokat.
	 */
	protected void telefon() {
		for (int i = 1; i <= 4; i++) {
			if (helyes != i) {
				valasz[i - 1] = "";
			}
		}
	}

	@Override
	/**
	 * toString metodus felteszi a kerdest.
	 */
	public String toString() {
		return kerdes + "\r\n" + valasz[0] + "\r\n" + valasz[1] + "\r\n"
				+ valasz[2] + "\r\n" + valasz[3] + "\r\n";
	}

	public int getHelyes() {
		return helyes;
	}

	public void setHelyes(int helyes) {
		this.helyes = helyes;
	}

}

